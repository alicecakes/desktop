import React from 'react';
import { Link } from 'react-router-dom';
import routes from '../constants/routes';
import styles from './Home.css';
import { dbQuery } from '../database/client';
import { Row, Col, Button, Form, Input, Icon, message } from 'antd';
import autobind from 'autobind-decorator';
import { findIndex, isEmpty } from 'lodash';

export default Form.create()(class Home extends React.PureComponent {

  state = {
    edit: false,
    editId: null,
    data: [],
  };

  componentWillMount() {
    this.fetch();
  }

  async fetch() {
    this.setState({ 
      data: await dbQuery({ 
        model: 'Contact',
        action: 'find',
        query: {},
      }),
    });
  }
  
  async save(values) {
    return dbQuery({
      model: 'Contact',
      action: 'save',
      query: values,
    });
  }

  edit(id) {
    this.setState({ edit: true, editId: id });
    this.props.form.setFieldsValue({
      ...this.state.data[findIndex(this.state.data, { _id: id })]
    });
  }

  async update() {
    if (isEmpty(this.state.editId)) {
      return;
    }

    this.props.form.validateFields(async (err, values) => {
      if (!err) {
        try {
          await dbQuery({
            model: 'Contact',
            action: 'findByIdAndUpdate',
            query: [this.state.editId, values],
          });

          this.setState({ edit: false, editId: null });
          this.props.form.resetFields();

          this.fetch();
          message.success('Контакт успешно обновлен');
        } catch (exception) {
          message.error(exception);
        }
      }
    });
  }

  async delete(id) {
    try {
      await dbQuery({
        model: 'Contact',
        action: 'findByIdAndDelete',
        query: id,
      });

      this.fetch();
      message.success('Контакт успешно удален');
    } catch (exception) {
      message.error(exception);
    }
  }

  @autobind
  async handleSubmit(event) {
    event.preventDefault();

    this.props.form.validateFields(async (err, values) => {
      if (!err) {
        if (this.state.edit) {
          console.log(this.state.editId);
          try {
            await this.update();
          } catch (exception) {
            message.error(exception);
          }
        } else {
          try {
            const saved = await this.save(values);
            this.fetch();
            this.props.form.resetFields();
          } catch (exception) {
            message.error(exception);
          }
        }
      }
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const { data } = this.state;

    return (
      <div className={styles.container} data-tid="container">
        <Row>
          <Col span={12}>
            <b>Контакты:</b>

            {data.length > 0 ? (
              data.map((contact, index) => (
                <div className={styles['list-item']} key={index}>
                  {contact.firstName}&nbsp;{contact.lastName}&nbsp;<b>+{contact.phoneNumber}</b>&nbsp;
                  (<span className={styles.remove} onClick={this.edit.bind(this, contact._id)}>Редактировать</span>)&nbsp;
                  (<span className={styles.remove} onClick={this.delete.bind(this, contact._id)}>Удалить</span>)
                </div>
              ))
            ) : <div className={styles['no-data']}>Нет контактов</div>}
          </Col>
          <Col span={12}>
            <Form onSubmit={this.handleSubmit}>
              <Form.Item label='Имя'>
                {getFieldDecorator('firstName', {
                  rules: [{ required: true, message: 'Поле обязательно' }],
                })(
                  <Input />
                )}
              </Form.Item>
              <Form.Item label='Фамилия'>
                {getFieldDecorator('lastName', {
                  rules: [{ required: true, message: 'Поле обязательно' }],
                })(
                  <Input />
                )}
              </Form.Item>
              <Form.Item label='Мобильный телефон'>
                {getFieldDecorator('phoneNumber', {
                  rules: [{ required: true, message: 'Поле обязательно' }],
                })(
                  <Input />
                )}
              </Form.Item>

              <Form.Item lable='Фамилия'>
                <Button type='primary' htmlType='submit'>{this.state.edit ? 'Обновить' : 'Добавить'}</Button>
              </Form.Item>
            </Form>
          </Col>
        </Row>
      </div>
    );
  }

});
