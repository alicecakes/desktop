const ipc = require('electron').ipcRenderer;

export function dbQuery(options) {
  ipc.send('db', options);
  
  return new Promise((resolve, reject) => {
    ipc.on('db-response', (event, { err, data }) => {
      if (err) {
        reject(err);
        return;
      }
  
      try {
        resolve(JSON.parse(data));
      } catch (exception) {
        reject('Не удалось выполнить запрос');        
      }
    });
  });
}