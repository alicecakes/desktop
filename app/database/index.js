import mongoose from 'mongoose';

const ipc = require('electron').ipcMain;

// pass: 1XGnh2dxbb1FMiYu
mongoose.connect('mongodb+srv://Admin:1XGnh2dxbb1FMiYu@cluster0-pkdn0.mongodb.net/test?retryWrites=true', { useNewUrlParser: true });

const models = {
  Contact: mongoose.model('Contact', {
    firstName: {
      type: String,
      set: (value) => value.trim(),
    }, 
    lastName: {
      type: String,
      set: (value) => value.trim(),
    },
    phoneNumber: {
      type: String,
      set: (value) => value.trim().replace('+', ''),
    }
  }),
};

ipc.on('db', (event, request) => {
  const {model, action, query} = request;
  const sendResult = (err, data) => event.sender.send('db-response', { err, data: JSON.stringify(data) });

  if (!models.hasOwnProperty(model)) {
    event.sender.send('db-response', { err: `Модель "${mode}" не найдена.` });
    return;
  }

  switch (action) {
    case 'save': {
      new models[model]({ _id: new mongoose.Types.ObjectId(), ...query })[action]()
        .then((data) => sendResult(null, data))
        .catch((err) => sendResult(err, null));
      return;
    }
    case 'findByIdAndUpdate': {
      models[model][action](...query).exec(sendResult);
      return;
    }
    default: {
      try {
        models[model][action](query).exec(sendResult);
      } catch (exception) {
        event.sender.send('db-response', { err: `Не удалось выполнить запрос "${action}" для модели "${model}".` });
      }
      return;
    }
  }
});